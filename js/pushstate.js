jQuery(function ($) {
	let site = 'PushState';

	const loadContent = (url) => {
		$('#conteudo').fadeOut('slow', function(data) {
			$.get(url).done(function (data) {
		 		$("#conteudo").html(data).fadeIn('slow');
		 	});
		});
	}

	$('.navbar ul li a').on('click', function (e) {
		e.preventDefault();

		var $this = $(this);
		var url = $this.attr("href");
		var titulo = $this.text();

		titulo = site + ' - ' + titulo.charAt(0).toUpperCase() + titulo.substr(1).toLowerCase();

		history.pushState({
			url: url,
			title: titulo
		}, titulo, url);

		document.title = titulo;

		loadContent(url);

		$('ul li').removeClass('is-active');
		$this.parent().addClass('is-active');
	});

	$(window).on('popstate', function (e) {
		var state = e.originalEvent.state;
		if (state !== null) {
			document.title = state.title;
			loadContent(state.url);
		} else {
			document.title = 'JavaScript PushState';
			loadContent('oque');
		};
	});
});