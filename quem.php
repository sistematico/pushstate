<?php include dirname(__FILE__) . '/inc/config.php';
if (!$isXHR) {
    $titulo = 'PushState - Contato';
    include dirname(__FILE__) . '/inc/cabecalho.php';
} ?>

<h1 class="title is-2">Créditos</h1>
<h2 class="subtitle is-4">Como isso foi possível.</h2>
<p>
    <aside class="menu">
        <p class="menu-label">Criado com</p>
        <ul class="menu-list">
            <li><a href="https://php.net">PHP</a></li>
            <li><a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript">JavaScript</a></li>
            <li><a href="https://jquery.com">jQuery</a></li>
            <li><a href="https://bulma.io">Bulma</a></li>
            <li><a href="https://plyr.io">Plyr</a></li>
        </ul>
    </aside>
</p>
<br />
<p>
    <aside class="menu">
        <p class="menu-label">Dicas</p>
        <ul class="menu-list">
            <li><a href="https://avexdesigns.com/responsive-youtube-embed/">Avex Designs</a></li>
        </ul>
    </aside>
</p>

<?php if (!$isXHR) {
  include dirname(__FILE__) . '/inc/rodape.php';
} ?>