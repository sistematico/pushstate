                        </div>
                    </div>
		    	</div> <!-- ./Columns -->
			</div> <!-- ./Container -->
		</div> <!-- ./ Hero Body -->
        <div class="hero-foot">
            <div class="container has-text-centered">
                <p>
                    &copy; 2019 Lucas Saliés Brum. Orgulhosamente hospedado por
                    <a href="https://www.owned-networks.net/client_area/aff.php?aff=11">Owned Networks</a>.
                    Fontes no <a href="https://gitlab.com/sistematico/pushstate">Gitlab</a>.
                </p>
            </div>
        </div>
    </section>
	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/pushstate.js"></script>
	<script src="js/burger.js"></script>
    <script src="js/tabs.js"></script>
    <script src="js/plyr.min.js"></script>
    <script src="js/player.js"></script>
</body>
</html>