<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>JavaScript PushState/PopState</title>
    <link rel="stylesheet" href="https://unpkg.com/bulma@0.7.5/css/bulma.min.css">
    <link rel="stylesheet" href="css/fontawesome-5.11.2.min.css">
    <link rel="stylesheet" href="css/plyr.css">
    <link rel="stylesheet" href="css/player.css">
    <link rel="shortcut icon" href="img/favicon.ico">
</head>
<body>
    <section class="hero is-fullheight is-default is-bold">
        <div class="hero-head">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="/">
                            <img src="img/flying-saucer.png" alt="PushState">&nbsp;PushState
                        </a>
                        <span class="navbar-burger burger" data-target="navbarMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navbarMenu" class="navbar-menu">
                        <div class="navbar-end">
                            <div class="tabs is-right">
                                <ul>
                                    <li class="is-active"><a href="oque">o que</a></li>
                                    <li><a href="como">como</a></li>
                                    <li><a href="quem">quem</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-5">

                        <div class="tabs is-toggle">
                            <ul class="audiovideo">
                                <li class="is-active">
                                    <a id="toggleAudio">
                                        <span class="icon is-small"><i class="fas fa-music" aria-hidden="true"></i></span>
                                        <span>Audio</span>
                                    </a>
                                </li>
                                <li>
                                    <a id="toggleVideo">
                                        <span class="icon is-small"><i class="fas fa-film" aria-hidden="true"></i></span>
                                        <span>Video</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="plyr__video-embed" id="video" style="display: none">
                            <iframe
                                src="https://www.youtube.com/embed/4kHl4FoK1Ys?origin=https://pushstate.lucasbrum.net&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1"
                                allowfullscreen
                                allowtransparency
                                allow="autoplay"
                            ></iframe>
                        </div>

                        <div class="player-container">
                            <span id="title"></span>
                            <audio id="audio" crossorigin playsinline>
                                <source id="audioSource" src="https://stream.radiochat.com.br:8010/principal" type="audio/mp3">
                            </audio>
                        </div>
                    </div>
                    <div class="column is-6 is-offset-1">
                        <div id="conteudo">