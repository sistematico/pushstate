# PushState

Um exemplo simples de site usando a técnica PushState/PopState do JavaScript com PHP.

### O que é

Usando essa técnica o site recarrega somente a URI da página, título e conteúdo de uma div, sem cortar a reprodução de um vídeo, audio ou carregamento de outros blocos dinâmicos do site.

### Usando

Acesse o site de exemplo, dê play no vídeo, clique nos links do menu(Início, Sobre, Contato).

### Instalação

#### Ambiente de desenvolvimento com Docker

https://gitlab.com/snippets/1843689

### Demonstração

https://pushstate.lucasbrum.net

### Referências

- https://code.tutsplus.com/tutorials/how-to-load-in-and-animate-content-with-jquery--net-26
- https://codyhouse.co/gem/animated-page-transition
- https://stephanwagner.me/loading-spinner-with-animation
- https://matthewjamestaylor.com/blog/perfect-2-column-right-menu.htm

## Ajude

Doe qualquer valor através do <a href="https://pag.ae/bfxkQW"><img src="https://img.shields.io/badge/doe-pagseguro-green"></a> ou <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWHJL387XNW96&source=url"><img src="https://img.shields.io/badge/doe-paypal-blue"></a>

## ScreenShot

![Screenshot][screenshot]

[screenshot]: https://gitlab.com/sistematico/pushstate/raw/master/pushstate.png "ScreenShot"