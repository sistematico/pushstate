<?php include dirname(__FILE__) . '/inc/config.php';
if (!$isXHR) {
    $titulo = 'PushState';
    include dirname(__FILE__) . '/inc/cabecalho.php';
} ?>
<h1 class="title is-2">JavaScript PushState/PopState</h1>
<h2 class="subtitle is-4">O que é? Para que serve?</h2>
<br>
<p>
    JavaScript PushState/PopState é um método de desenvolvimento web que utiliza vantagens da History API do browser, podendo injetar informações no histórico do browser de forma assíncrona(um url/uri por exemplo).<br /><br />
    Dessa forma, ao clicar em um link a página não é recarregada por completo, apenas alguns campos são renovados, como URL e uma DIV.
</p>
<?php if (!$isXHR) {
    include dirname(__FILE__) . '/inc/rodape.php';
} ?>