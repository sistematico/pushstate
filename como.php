<?php
include dirname(__FILE__) . '/inc/config.php';
if (!$isXHR) {
    $titulo = 'PushState - Sobre';
    include dirname(__FILE__) . '/inc/cabecalho.php';
}
?>
<h1 class="title is-2">Usando</h1>
<h2 class="subtitle is-4">Como usar o site</h2>
<br>
<p>
    Dê play no audio ou vídeo ao lado e navegue pelos links 'o que', 'como' e 'quem'.<br />
    A barra de endereços será alterada assim como o conteúdo desta div, porém o vídeo ou audio não será interrompido(eu espero! =D).
</p>
<?php if (!$isXHR) {
    include dirname(__FILE__) . '/inc/rodape.php';
} ?>